<?php
error_reporting(0);
session_start();
if($_SESSION['username'] == ""){
header("Location: index.php");}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Free Bootstrap Admin Template : Binary Admin</title>
        <!-- BOOTSTRAP STYLES-->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FONTAWESOME STYLES-->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- MORRIS CHART STYLES-->
        <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
        <link href="assets/css/custom.css" rel="stylesheet" />
        <!-- GOOGLE FONTS-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
           <div id="wrapper">
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Home Page</a> 
                </div>
                <div style="color: white;
                     padding: 15px 50px 5px 50px;
                     float: right;
                     font-size: 16px;"> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
            </nav>   
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">
                           <?php
error_reporting(0);
$con = mysqli_connect("localhost", "id2172541_harmonics", "killbill100", "id2172541_harmonics");
$username = $_SESSION['username'];
$sql = "SELECT img FROM users WHERE username='$username'";
$result = $con->query($sql);
$image = mysqli_fetch_array($result);
$imagepath = $image['img'];
if($imagepath != "images/" && $imagepath != ""){
echo " <img src='".$imagepath."' class='user-image img-responsive'/>";
}else{
echo "<img src='assets/img/find_user.png' class='user-image img-responsive'/>";
}
?>
                        </li>


                        <li>
                            <a class="active-menu"  href="index.html"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                        </li>
		

                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Exercises<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                               <?php
error_reporting(0);
$con = mysqli_connect("localhost", "id2172541_harmonics", "killbill100", "id2172541_harmonics");
$username = $_SESSION['username'];
$sql = "SELECT unlockq FROM users WHERE username='$username'";
$result = $con->query($sql);
$unlockedq = mysqli_fetch_array($result);
echo "<li>
      <a href='q1pdf.php' style='color:green;'>Question 1</a>
      </li>";
if($unlockedq['unlockq'] >= 2){
echo "<li>
      <a href='q2.php' style='color:green;'>Question 2</a>
      </li>";
}else{
echo "<li>
      <a href='#' style='color:red;'>Question 2</a>
      </li>";
}
if($unlockedq['unlockq'] >= 3){
echo "<li>
      <a href='q3.php' style='color:green;'>Question 3</a>
      </li>";
}else{
echo "<li>
      <a href='#' style='color:red;'>Question 3</a>
      </li>";
}
?>
                            </ul>
                        </li>  
                    </ul>

                </div>

            </nav>  
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper" >


                <div class="col-md-12">
                    <h2>Lecture 1</h2>   
                    <!-- <h5>Welcome Jhon Deo , Love to see you back. </h5> -->
                </div>

                <!-- /. ROW  -->
                <!-- /. ROW  -->
<hr />
<div class="row"> 
<div class="jumbotron">                     
<div class="panel panel-default">
<div class="panel-heading">
Quiz
</div>
<div class="panel-body">


<form method="post" action="results.php">
<p class="question">1. You are more likely to develop Sjögren's syndrome:</p>
<ul class="answers">
<input type="radio" name="q1" value="0" id="q1a" required><label for="q1a">As a teenager</label><br/>
<input type="radio" name="q1" value="0" id="q1b" required><label for="q1b">During childbearing years</label><br/>
<input type="radio" name="q1" value="1" id="q1c" required><label for="q1c">After menopause</label><br/>
<input type="radio" name="q1" value="0" id="q1d" required><label for="q1d">During pregnancy</label><br/>
</ul>

<p class="question">2. The hallmark symptoms of Sjögren's are:</p>
<ul class="answers2">
<input type="radio" name="q2" value="1" id="q2a" required><label for="q2a">Dry eyes and mouth</label><br/>
<input type="radio" name="q2" value="0" id="q2b" required><label for="q2b">Fatigue and headache</label><br/>
<input type="radio" name="q2" value="0" id="q2c" required><label for="q2c">Cavities and vaginal dryness</label><br/>
<input type="radio" name="q2" value="0" id="q2d" required><label for="q2d">Joint pain and organ damage</label><br/>
</ul>

<p class="question">3. Sjögren's:</p>
<ul class="answers2">
<input type="radio" name="q3" value="0" id="q3a" required><label for="q3a">Can be prevented, if you follow a careful diet and don't smoke</label><br/>
<input type="radio" name="q3" value="1" id="q3b" required><label for="q3b">Can be treated, but not prevented</label><br/>
<input type="radio" name="q3" value="0" id="q3c" required><label for="q3c">Can neither be prevented nor treated</label><br/>
<input type="radio" name="q3" value="0" id="q3d" required><label for="q3d">Cannot be diagnosed</label><br/>
</ul>

<p class="question">4. Dry mouth can lead to:</p>
<ul class="answers2">
<input type="radio" name="q4" value="0" id="q4a" required><label for="q4a">Cavities</label><br/>
<input type="radio" name="q4" value="0" id="q4b" required><label for="q4b">Fungal infections such as oral candidiasis</label><br/>
<input type="radio" name="q4" value="0" id="q4c" required><label for="q4c">Impaired ability to eat or speak</label><br/>
<input type="radio" name="q4" value="1" id="q4d" required><label for="q4d">All of the above</label><br/>
</ul>

<p class="question">5. A diagnostic work-up for Sjögren's will likely include:</p>
<ul class="answers2">
<input type="radio" name="q5" value="0" id="q5a" required><label for="q5a">A discussion of symptoms</label><br/>
<input type="radio" name="q5" value="0" id="q5b" required><label for="q5b">A physical examination</label><br/>
<input type="radio" name="q5" value="0" id="q5c" required><label for="q5c">Blood tests for Sjögren's markers</label><br/>
<input type="radio" name="q5" value="1" id="q5d" required><label for="q5d">All of the above</label><br/>
</ul>

<input type="hidden" name="question" value="1">
<input type="submit" value="Submit" id="res"><br/>
</form>
</div>
</div>            
</div>               
</div>
<!-- /. ROW  -->   
<form action="q1pdf.php">
<input type="image" src="images/previous.jpg" width="20%" height="15%"/>
</form>
<!-- /. ROW  -->           

<!-- /. PAGE INNER  -->
</div>				
<!-- /. PAGE WRAPPER  -->
</div>

<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="quiz-1.js"></script>
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="assets/js/morris/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>


</body>
</html>