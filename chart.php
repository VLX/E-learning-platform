<?php
session_start();

$con = mysqli_connect('localhost', 'id2172541_harmonics', 'killbill100', 'id2172541_harmonics');

$username = $_SESSION['username'];

$sql = "SELECT * FROM progress WHERE username='$username'";
$result = $con->query($sql);
$datach = "";
		while($row = mysqli_fetch_array($result)){
			//$year = idate('Y', $row['date']);
			//$month = idate('m', $row['date']);
			//$day = idate('d', $row['date']);
			//$hour = idate('H', $row['date']);
			//$min = idate('i', $row['date']);
			$score = $row['score'];
			$datep = $row['date'];
			$datach = $datach."chartData.push({date: '".$datep."',visits: ".$score."});";
			}
echo"			
<html>
<head>
<!-- Styles -->
<style>
#chartdiv {
	width	: 100%;
	height	: 500px;
}																	
</style>

<!-- Resources -->
<script src='https://www.amcharts.com/lib/3/amcharts.js'></script>
<script src='https://www.amcharts.com/lib/3/serial.js'></script>
<script src='https://www.amcharts.com/lib/3/plugins/export/export.min.js'></script>
<link rel='stylesheet' href='https://www.amcharts.com/lib/3/plugins/export/export.css' type='text/css' media='all' />
<script src='https://www.amcharts.com/lib/3/themes/light.js'></script>

<!-- Chart code -->
<script>
var chartData = generateChartData();
var chart = AmCharts.makeChart('chartdiv', {
    'type': 'serial',
    'theme': 'light',
    'marginRight': 80,
    'autoMarginOffset': 20,
    'marginTop': 7,
    'dataProvider': chartData,
    'valueAxes': [{
        'axisAlpha': 0.2,
        'dashLength': 1,
        'position': 'left'
    }],
    'mouseWheelZoomEnabled': true,
    'graphs': [{
        'id': 'g1',
        'balloonText': '[[value]]',
        'bullet': 'round',
        'bulletBorderAlpha': 1,
        'bulletColor': '#FFFFFF',
        'hideBulletsCount': 50,
        'title': 'red line',
        'valueField': 'visits',
        'useLineColorForBulletBorder': true,
        'balloon':{
            'drop':true
        }
    }],
    'chartScrollbar': {
        'autoGridCount': true,
        'graph': 'g1',
        'scrollbarHeight': 40
    },
    'chartCursor': {
       'limitToGraph':'g1'
    },
    'categoryField': 'date',
    'categoryAxis': {
        'parseDates': true,
        'axisColor': '#DADADA',
        'dashLength': 1,
        'minorGridEnabled': true
    },
    'export': {
        'enabled': true
    }
});

chart.addListener('rendered', zoomChart);
zoomChart();

// this method is called when chart is first inited as we listen for 'rendered' event
function zoomChart() {
    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
    chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
}


// generate some random data, quite different range
function generateChartData() {
    var chartData = [];
".$datach."
    return chartData;
}
</script>
</head>
<body>
<!-- HTML -->
<div id='chartdiv'></div>	
</body>
</html>";
?>